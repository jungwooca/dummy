// program runs line by line sequencially. 
// line 1 then line 2 then 3 ... (except that in JS function hoisting put function at the beginning of the code) 
// what is the result?  
// we know it is executed in the order A , B, b, C, D
//which one printed first? 2 or 4  which one printed last 1 or 3?  
// explain with terms stack, event queue, LILO, FIFO. then run node async.js then time it.


setTimeout(function(){ console.log("1. i am inside of a waiting 5 sec ") }, 5000);  //A
console.log("2. I am called after outside setTimeout is executed");  //B

async function b(){ 
    await setTimeout(function(){ console.log("3. i am inside of a waiting 2 sec ")} , 3000);  //C
    console.log("4. I am called after outside setTimeout is executed");   //D
}
b();